<?
// 1 задание 3-го ДЗ 2 курса
include_once ("{$_SERVER['DOCUMENT_ROOT']}/autoloader.php");

Use Tags\SingleTag as Single;
Use Tags\PairTag as Pair;

$check = new Single('input');
$check->setAttr(['type' => 'checkbox', 'id' => 'check'])->setAttr('checked');

$label = new Pair('label');
$label->setAttr('for', 'check')->setContent('чекбокс');

// 3 задание 3-го ДЗ 2 курса
$pair = new Pair('div');
$pair->setAttr('style', 'float: left')->setContent('Начало текстового контента');
$pair->setContent([new Single('br'), $check, $label], true);
$pair->setContent(new Single('br'), true)->setContent('Завершение текстового контента', true);
echo $pair; //   2 задание 3-го ДЗ 2 курса