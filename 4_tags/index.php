<?
// 1 задание 3-го ДЗ 2 курса
include_once ("{$_SERVER['DOCUMENT_ROOT']}/autoloader.php");

Helpers\Cookies::add('user', '1');
echo Helpers\Cookies::read('user');

$input = new Tags\Single('input');
echo $input->attr('type', 'button')
    ->attr('value', 'Кнопка')
    ->attr('disabled')
    ->render();

$a = new Tags\Pair('a');
echo $a->html('Some')->attr('href', '#')->render();

