<?php

namespace Tags;
class Single extends Tag
{
    public function __construct($name)
    {
        parent::__construct($name);
        $this->pattern = '<{{name}} {{params}}>';
    }
}