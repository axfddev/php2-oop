<?php

namespace Tags;
class Img extends Tag
{
    public function __construct($name)
    {
        parent::__construct($name);
        $this->requiredAttrs[] = 'src';
    }
}
