<?php

namespace Tags;
class Pair extends Tag
{
    protected $innerHTML;

    public function __construct($name)
    {
        parent::__construct($name);
        $this->pattern = '<{{name}} {{params}}>{{content}}</{{name}}>';
        $this->innerHTML = '';
    }

    /*
        ->html('str')
        ->html(new SignleTag())
        ->html([new SignleTag(), new SignleTag(), new SignleTag()])
    */

    public function html($text)
    {
        $this->innerHTML = $text;
        return $this;
    }

    public function render()
    {
        $str = parent::render();
        $str = str_replace('{{content}}', $this->innerHTML, $str);
        return $str;
    }
}