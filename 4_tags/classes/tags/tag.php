<?php

namespace Tags;
abstract class Tag
{
    protected $name; // img
    protected $pattern;
    protected $attrs; // ['src' => '1.jpg']

    public function __construct($name)
    {
        $this->name = $name;
        $this->pattern = '';
        $this->attrs = [];
        $this->requiredAttrs = [];
    }

    public function attr($name, $value = true)
    {
        $this->attrs[$name] = $value;
        return $this;
    }

    /* href=>1, title=2 */
    public function attrs($list)
    {

    }

    public function render()
    {
        $str = str_replace('{{name}}', $this->name, $this->pattern);

        foreach ($this->requiredAttrs as $attr) {
            if (!isset($this->attrs[$attr])) {
                // throw new Exception
                exit("required attr $attr in tag {$this->name}");
            }
        }

        $attrTmp = [];

        foreach ($this->attrs as $name => $value) {
            if ($value === true) {
                $attrTmp[] = "$name";
            } else if ($value === false) {
                ;
            } else {
                $attrTmp[] = "$name=\"$value\"";
            }
        }

        $attrStr = implode(' ', $attrTmp);
        $str = str_replace('{{params}}', $attrStr, $str);

        return $str;
    }
}