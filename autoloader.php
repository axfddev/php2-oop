<?
spl_autoload_register(function ($className) {
    $file = "classes/" . strtolower($className) . ".php";
    if (file_exists($file)) {
        include_once($file);
    } else {
        return false;
    }
});