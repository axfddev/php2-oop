<?
namespace Cookie;
class Cookies
{
    public static function add($name, $value, $lifeTime = null, $path = '/')
    {
        if ($lifeTime === null || $lifeTime === 0) {
            $time = 0;
        } else {
            $time = time() + $lifeTime;
        }

        setcookie($name, $value, $time, $path);
        $_COOKIE[$name] = $value;
    }

    public static function read($name)
    {
        return $_COOKIE[$name] ?? null;
    }

    public static function slice($name)
    {
        $coockie = self::read($name);
        self::remove($name);
        return $coockie;
    }

    public static function remove($name)
    {
        setcookie($name, '', -1);
        if (isset($_COOKIE[$name])) unset ($_COOKIE[$name]);
    }

    public static function removeAll()
    {
        foreach ($_COOKIE as $cName => $cVal) {
            self::remove($cName);
        }
    }
}
