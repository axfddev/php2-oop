<?

namespace Tags;
class PairTag extends Tag
{
    protected $content;
    protected $innerTags;

    public function __construct($name)
    {
        parent::__construct($name);
        $this->pattern = '<{{name}} {{params}}>{{content}}</{{name}}>';
        $this->content = '';
    }

    public function setContent($content = '', $add = false)
    {
        {
            if (is_array($content)) {
                if (!$add) $this->setContent('');
                foreach ($content as $tag) {
                    $this->setContent($tag, true);
                }
            } elseif (is_object($content)) {
                $this->setContent($content->render(), $add);
            } else {
                if ($add) {
                    $this->content .= $content;
                } else {
                    $this->content = $content;
                }
            }
        }
        return $this;
    }

    public function render($print = false)
    {
        $str = parent::render();
        $str = str_replace('{{content}}', $this->content, $str);
        if ($print) echo $str;
        return $str;
    }
}	
