<?

namespace Tags;
abstract class Tag
{
    protected $name;
    protected $pattern;
    protected $attrs;

    public function __construct($name)
    {
        $this->name = $name;
        $this->attrs = [];
    }

    public function setAttr($attr, $value = null)
    {
        if (!is_null($attr)) {
            if (is_array($attr)) {
                $this->attrs = array_merge($this->attrs, $attr);
            } else {
                $this->attrs[$attr] = $value;
            }
        }
        return $this;
    }

    public function clearAttr()
    {
        $this->attrs = [];
        return $this;
    }

    protected function attrToString()
    {
        $attrTmp = [];
        foreach ($this->attrs as $name => $value) {
            if (is_null($value)) {
                $attrTmp[] = "$name";
            } else {
                $attrTmp[] = "$name=\"$value\"";
            }
        }
        return implode(' ', $attrTmp);
    }


    public function render($print = false)
    {
        $str = str_replace('{{name}}', $this->name, $this->pattern);
        $str = str_replace('{{params}}', $this->attrToString(), $str);
        if ($print) echo $str;
        return $str;
    }

    public function __toString()
    {
        return $this->render();
    }
}	
