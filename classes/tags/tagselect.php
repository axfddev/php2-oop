<?
namespace Tags;
class TagSelect extends multitag
{
    public function __construct()
    {
        parent::__construct('select', 'option');
    }

    public function setInnerTags($arContent)
    {
        $tempContent = [];
        $item = new PairTag($this->innerTagName);
        foreach ($arContent as $innerItem) {
            $item->clearAttr();
            if (is_array($innerItem)) {
                $item->setAttr($innerItem['attr']);
                $item->setAttr('value', $innerItem['value'] /*?? $innerItem['text']*/);
                $item->setContent($innerItem['text']);
            } else {
                $item->setContent($innerItem);
                $item->setAttr('value', $innerItem);
            }
            $this->setContent($item, true);
        }
        return $this;
    }

    public function initEasy($count = 1, $text = '', $numeration = false)
    {
        $tempContent = [];
        for ($i = 1; $i <= $count; $i++) {
            $tempContent[] = array(
                'text' => (trim($text . ($numeration ? " $i" : ""))),
                'value' => $i
            );
        }
        $this->setInnerTags($tempContent);
        return $this;
    }

}
