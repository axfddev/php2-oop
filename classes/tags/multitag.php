<?

namespace Tags;
class MultiTag extends PairTag
{
    protected $innerTagName;

    public function __construct($name, $innerTagName)
    {
        parent::__construct($name);
        $this->innerTagName = $innerTagName;
    }

    public function setInnerTags($arContent)
    {
        $item = new parent($this->innerTagName);

        foreach ($arContent as $innerItem) {
            $item->clearAttr();
            if (is_array($innerItem)) {
                $item->setAttr($innerItem['attr']);
                $item->setContent($innerItem['text']);
            } else {
                $item->setContent($innerItem);
            }
            $this->setContent($item, true);
        }
        return $this;
    }

    public function initEasy($count = 1, $text = '', $numeration = false)
    {
        $tempContent = [];
        for ($i = 1; $i <= $count; $i++) {
            $tempContent[] = (trim($text . ($numeration ? " $i" : "")));
        }
        $this->setInnerTags($tempContent);
        return $this;
    }

}
